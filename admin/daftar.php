<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Beranda</title>
<link rel="stylesheet" href="styles.css" type="text/css" />
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
</head>

<body>

		<section id="body" class="width">
			<aside id="sidebar" class="column-left">

			<header>
				<h1><a href="#">Perpustakaan</a></h1>

				<h2>Selamat datang di perpustakaan</h2>
				
			</header>

			<nav id="mainnav">
  				<ul>
              <li class="selected-item"><a href="index.html">Master Anggota</a></li>
              <li><a href="#">Master Buku</a></li>
              <li><a href="#">Data Peminjaman</a></li>
                        	</ul>
			</nav>

			
			
			</aside>
			<section id="content" class="column-right">
         <a href="Index.html"><h3>Data Anggota</a> > Daftar </h3>

<link rel="stylesheet" href="css/daftar.css" type="text/css" />
<div class="container">  
  <form id="contact" action="" method="post">
 
    <h4>Silahkan lengkapi Bidodata Anda</h4>
    <fieldset>
      <input placeholder="Nama Lengkap" type="text" tabindex="1" required autofocus>
    </fieldset>
    <fieldset>
      <input placeholder="Alamat Email" type="email" tabindex="2" required>
    </fieldset>
    <fieldset>
      <input placeholder="Nomor Handphone (optional)" type="tel" tabindex="3" required>
    </fieldset>
    <fieldset>
      <input placeholder="Tempat Lahir" type="url" tabindex="4" required>
    </fieldset>
    <fieldset>
      <input placeholder="Tanggal Lahir" type="url" tabindex="5" required>
    </fieldset>
    <fieldset>
      <textarea placeholder="Alamat" tabindex="6" required></textarea>
    </fieldset>
    <fieldset>
      <button name="submit" type="submit" id="contact-submit" data-submit="...Sending">Submit</button>
    </fieldset>
 
  </form>
</div>

		</article>

			
			<footer class="clear">
				
			</footer>

		</section>

		<div class="clear"></div>

	</section>
	

</body>
</html>
